\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {ngerman}
\contentsline {chapter}{\numberline {1}Einleitung}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Motivation}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}EnEx-nExT}{2}{section.1.2}
\contentsline {chapter}{\numberline {2}Aufgabenstellung}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Beschreibung der Aufgabe}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}Beschreibung der vorhandenen Infrastruktur/ Versuchsaufbau EnEx-nExT}{3}{section.2.2}
\contentsline {section}{\numberline {2.3}Anforderungen aus der Aufgabenstellung}{4}{section.2.3}
\contentsline {chapter}{\numberline {3}Beschreibung und Festlegung der zu benutzenden Messverfahren und Technologien}{6}{chapter.3}
\contentsline {section}{\numberline {3.1}Grundlagen zur Messung von Str\IeC {\"o}mungsgeschwindigkeiten}{6}{section.3.1}
\contentsline {section}{\numberline {3.2}Antriebe}{7}{section.3.2}
\contentsline {chapter}{\numberline {4}Entwicklung des Versuchsaufbaus}{8}{chapter.4}
\contentsline {section}{\numberline {4.1}Auslegung}{8}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Konstruktion Halterung}{8}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Auswahl des Schrittmotors}{9}{subsection.4.1.2}
\contentsline {chapter}{\numberline {5}Bewertung der Messeinrichtung}{11}{chapter.5}
