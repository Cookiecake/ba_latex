\chapter{Entwicklung des Versuchsaufbaus}
\section{Auslegung}
\subsection{Konstruktion Halterung}
Für das vermessen des Strömungsprofils mit  der Pitotrohrsonde ist es notwendig eine Einrichtung zu entwerfen, in der die Sonde aufgenommen werden kann und sich in dem zweidimensionalen Messraster verfahren lässt. Für diese Einrichtung lassen sich durch die gegebene Infrastruktur Randbedingungen definieren. Diese Bedingungen geben die Größe und die Funktion vor.  
%Abnehmebarkeit, Klemmen, Bezug auf platz in der Vakuumkammer, Stabilität, Sicherheitsabstand aus den Thermometern, Berechnung zur Platzierung des Pitotrohres
\subsection{Auswahl des Schrittmotors}
Zur Bewegung des Schlittens der ZRW-1040 Linearachse ist ein Schrittmotor zu bestimmen. Als wichtiges Auslegungskriterium ist das notwendige Motordrehmoment zu nennen. Wird ein Schrittmotor hier überlastet, kann es passieren, dass die Spulen überspringen und der Motor somit Schritte verliert. Damit wäre keine genaue Positionierung mehr möglich, da die Zählung der Schritte damit nicht mehr sichergestellt ist.  
Für die Berechnung des notwendigen Motordrehmomentes wird in \autocite{Mansius.2012} folgende Gleichung angegeben:
\begin{equation}
M_{besch}=M_{verz}=J_A*\alpha\\
\label{eq:Moment}
\end{equation}
mit:
\begin{equation}
J_A=m*\left( \frac{K_{VA}}{2*\pi}\right) ^2
\end{equation}
und:
\begin{equation}
\alpha=\frac{a*2*\pi}{K_{VA}}
\end{equation}
Dabei ist $J_A$ das Massenträgheitsmoment der Masse $m$ die von dem Motor auf dem Linearantrieb bewegt werden soll und $K_{VA}=\pi*d_{rad}$ die Vorschubkonstante. %Überprüfen wie der im Buch heißt
Dieser ist vom Hersteller mit \SI{66}{\mm\per U}\autocite{ZLW:TechnischDaten} angegeben. Das bedeutet für jede vollständige Umdrehung des Motors werden \SI{66}{\mm} auf der Achse von dem Schlitten ausgeführt.
Durch die Forderung HAL-060, dass der Linearantrieb GRW-0630 maximal \SI{1}{\kilo\gram} bewegen kann und selber ein Gewicht von \SI{\approx\,600}{\gram} besitzt, ist die zu bewegende maximale Masse im Vorhinein bekannt. Aus Gründen der Sicherheit und der unbekannten inneren Reibungsparameter des ZRW-1040 wird mit einer zu verfahrenden Masse von \SI{2}{\kilo\gram} gerechnet. Die benötigte Beschleunigung $a$ liegt bei \SI{0,04}{\meter\per\square\second} um die Anforderung HAL-030  zu erfüllen.
Damit läßt sich $J_A$ und $\alpha$ bestimmen zu:
\begin{align*}
J_A=\SI{2}{\kilo\gram}\*\left(\frac{\SI{66}{\mm\per U}}{2*\pi}\right)^2 = \SI{2,20677e-4}{\kilo\gram\square\meter}  \\
\alpha=\frac{\SI{0,04}{\meter\per\square\second}*2*\pi}{\SI{66}{\mm\per U}} = \SI{3,8}{\per\square\second}
\end{align*}
eingesetzt in \autoref{eq:Moment} lautet das notwendige Motordrehmoment:
\begin{equation*}
M= \SI{8,3625e-4}{\kilo\gram\square\meter\per\square\second} =\SI{0.00083}{\N\meter}
\end{equation*}
Mit diesem ermittelten Wert wird der zum Einsatzkommende Motor gesucht. Hier stellt sich heraus, dass sich hier der Schrittmotor der Größe NEMA17 der Firma Igus eignet. Er besitzt ein Haltemoment von \SI{0,5}{\N\meter}\autocite{NEMA17} welches für die Aufgabe ausreicht. Zusätzlich gibt es zu diesem Motor und dem bereits vorhandenen Linearantrieb eine passende Kupplung und eine Montagehalterung. 

 \begin{center}
 \begin{minipage}{\linewidth}% Damit Abbildung und Beschriftung zusammen bleiben.
 \centering
 \captionof{table}{wichtige Daten NEMA17\autocite{NEMA17}}\vspace{8pt}
 \begin{tabular}{cc}
 \toprule
 Bezeichnung & Wert\\
 \midrule
 Nennspannung & \SI{24\dots 48}{\V}\\
 Haltemoment & \SI{0,5}{\N\meter}\\
 Schrittwinkel & \SI{1,8}{\degree}\SI{\pm 5}{\percent}\\
 Gewicht & \SI{0,43}{\kg}\\
 Umgebungstemperatur & \SI{-10\dots 50}{\celsius}\\
 \bottomrule
 \end{tabular}
 \label{tab:nema17}
 \end{minipage}
 \end{center}
